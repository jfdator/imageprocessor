// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="James South">
//   Copyright (c) James South.
//   Licensed under the Apache License, Version 2.0.
// </copyright>
// <summary>
//   AssemblyInfo.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ImageProcessor")]
[assembly: AssemblyDescription("A library for on-the-fly processing of image files written in C#")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("James South")]
[assembly: AssemblyProduct("ImageProcessor")]
[assembly: AssemblyCopyright("Copyright � James South")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bdaae9bd-0dc8-4b06-8722-e2e0c9a74301")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.3.0.0")]
[assembly: AssemblyFileVersion("2.3.0.0")]

[assembly: InternalsVisibleTo("ImageProcessor.UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100e9844a5a18a9fb0bc7984d7efe16d3cde480871d4185de23d5b0dda80fe3e592fca84f4a91d159bef5d81e7d345ed2e7ed5e7b8754a28eddb2845d09a5b4994bfe9af8ba3766d8cf6ded75a43123192a6505a04987a074490c683b6ef63b3e36072af9a0b539310337bab4ea0b3216f0fb3d732df91582d6f831393a19bc769d")]
[assembly: InternalsVisibleTo("ImageProcessor.Web, PublicKey=0024000004800000940000000602000000240000525341310004000001000100e9844a5a18a9fb0bc7984d7efe16d3cde480871d4185de23d5b0dda80fe3e592fca84f4a91d159bef5d81e7d345ed2e7ed5e7b8754a28eddb2845d09a5b4994bfe9af8ba3766d8cf6ded75a43123192a6505a04987a074490c683b6ef63b3e36072af9a0b539310337bab4ea0b3216f0fb3d732df91582d6f831393a19bc769d")]
